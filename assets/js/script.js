    // portfolio
const menuproduct =[
        {
            category:"latest",
            imgSrc:"assets/image/portfolio/p1.jpg",
            h4:"minimal design"
            
        },
        {
            category:"popular",
            imgSrc:"assets/image/portfolio/p2.jpg",
            h4:"paint wall"
    
        },
        {
            category:"latest",
            imgSrc:"assets/image/portfolio/p3.jpg",
            h4:"female light"
    
        },
        {         category:"popular",
           imgSrc:"assets/image/portfolio/p4.jpg",
           h4:"fourth air"
        },
        {
            category:"following",
            imgSrc:"assets/image/portfolio/p5.jpg",
            h4:"multiply fowl"
    
        },
        {
            category:"upcoming",
           imgSrc:"assets/image/portfolio/p6.jpg",
           h4:"together sign"
        
        },
        {
            category:"upcoming following",
            imgSrc:"assets/image/portfolio/p7.jpg",
            h4:"green heaven"
    
        },
        {
           category:"following",
            imgSrc:"assets/image/portfolio/p8.jpg",
            h4:"fly male"
    
        },
       {
            category:"upcoming",
            imgSrc:"assets/image/portfolio/p9.jpg",
            h4:"season face"
    
        },
    ];
    
    const menuWrapper = document.querySelector(".card-img");
    const allBtns = document.querySelectorAll(".btn");
    const btnContainer = document.querySelector(".btn-container");
    
    btnContainer.addEventListener("click",(e)=>{
        // const btnTarget = e.target.classList.contains("btn");
        const btnId = e.target.dataset.id;
         allBtns.forEach(btn =>{
             if(btnId){
                btn.classList.remove("active-btn");
                e.target.classList.add("active-btn");
            }
         });
    });
    
    allBtns.forEach((btn) =>{
        btn.addEventListener("click",(e)=>{
           const id = e.currentTarget.dataset.id;
            const filterMenu = menuproduct.filter((item) =>{
                return item.category == id;
            });
            if (id == "all"){
                showingProduct(menuproduct);
        }else{
            showingProduct(filterMenu);
        }
    
            });
    });
    
    
    
    
    window.addEventListener("DOMContentLoaded",()=>{
        showingProduct(menuproduct);
    });
    
    const showingProduct = (arrayProducts) =>{
       const displayProduct = arrayProducts.map((p)=>{
        return `<div class="col-lg-4 col-md-6 filter-item" data-filter=" all latest">
        <div class="portfolio-box">
                <div class="single-portfolio pb-4">
                        <img class="img-fluid w-100" src="${p.imgSrc}" alt="">
                </div>
                <div class="short-info">
                        <h4>${p.h4}</h4>
                        <p>Animated, portfolio</p>
                </div>
        </div>
</div>
    `;
    })
    .join("");
    menuWrapper .innerHTML = displayProduct;
    }
      